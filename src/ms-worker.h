#ifndef __worker__h
#define __worker__h

//#include "mpicxx.h"
#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <solution.h>
#include <eval.h>

/**
  Multi objective worker
**/
template<class SolT>
class msWorker {
public:
  msWorker(Eval<SolT> & _eval) : evaluation(_eval) {
    rank = MPI::COMM_WORLD.Get_rank();
  }

  void run() {
    MPI::Status Stat;
    int size;
    const int Tag = 1;
    const int EndTag = 2;
    
    // get the size from the broadcast
    MPI::COMM_WORLD.Bcast(&size, 1, MPI::INT, 0);

    SolT solution;

    int sol[size];
    
    while (1) { // infinite until abort
      // receive a solution
      MPI::COMM_WORLD.Recv(sol, size, MPI::INT, 0, MPI::ANY_TAG, Stat);
        
      if (Stat.Get_tag() == EndTag) break;

#ifdef TRACE        
      printf("Slave %d is now evaluating : ", rank);
      for(int k = 0; k < size; k++) printf("%d, ", sol[k]);
      printf("...\n");
      fflush(stdout);
#endif
      
      // compute the fitness of the solution
      solution.resize(size);
      for(unsigned int i = 0; i < size; i++)
        solution[i] = sol[i];

      evaluation(solution);    
      double objVec[2];
      objVec[0] = solution.objective_vector[0];
      objVec[1] = solution.objective_vector[1];

      // send it
      MPI::COMM_WORLD.Send(objVec, 2, MPI::DOUBLE, 0, Tag);
    }
    
    return;
  }

protected:
  Eval<SolT> & evaluation;
  int rank;

};

#endif
