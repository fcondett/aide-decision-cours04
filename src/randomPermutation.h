#ifndef _randomPermutation_h
#define _randomPermutation_h

#include <solution.h>
#include <random>
#include <algorithm>

class RandomPermutation {
public:
	RandomPermutation(std::default_random_engine & _rng) : rng(_rng) {

	}

	void operator()(Solution<unsigned int> & solution) {
	  solution.objective_vector.resize(2);
	  
	  // random solution (permutation)
	  solution.resize(57);

	  for(unsigned int k = 0; k < 57; k++)
	    solution[k] = k;

	  std::shuffle(std::begin(solution), std::end(solution), rng);
	}

protected:
	std::default_random_engine & rng;
};

#endif