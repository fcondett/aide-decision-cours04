#ifndef __msSearch__h
#define __msSearch__h

#include "mpi.h"
//#include "mpicxx.h"
#include <vector>
#include <fstream>
#include <random>

#include <randomPermutation.h>

using namespace std;

/**
   Master/Slave distributed Biobjective MOEA/D
 **/
class MS_SEARCH {
public:
  MS_SEARCH(unsigned _size, unsigned _np, unsigned _duration)
  : size(_size), np(_np), duration(_duration) {
  }

  virtual void run() {
    // time duration of the run (in second)
    finishtime = time(NULL) + duration;        

    // request for reception
    MPI::Request req_recv[np];
    // request for send
    MPI::Request req_send[np];
    // MPI misc
    MPI::Status Stat;    
    const int Tag = 1;
    const int EndTag = 2;

    // buffers to send and receive solutions and objective vectors
    vector<int *> features(np);
    vector<double *> res(np);
    for(unsigned k = 0; k < np; k++) {
      features[k] = new int[size];
      res[k] = new double[2];
    }

    // random solutions
    vector<Solution<unsigned int> > solutions(np);

    auto rng = std::default_random_engine {};
    RandomPermutation init(rng);

    for(unsigned k = 1; k < np; k++) {
      init(solutions[k]);
    }

    // send in non-blocking mode (SV: check buffer: same buffer for all messages)
    /* from doc mpi: https://www.open-mpi.org/doc/v1.8/man3/MPI_Isend.3.php
       "A nonblocking send call indicates that the system may start copying data out of the send buffer. The sender should not modify any part of the send buffer after a nonblocking send operation is called, until the send completes."
       donc il vaut des buffers "features" differents pour chaque mutant, pour éviter les surprises.
    */
    for(unsigned k = 0; k < np; k++) {
      for(unsigned l = 0; l < size; l++)
        features[k][l] = solutions[k][l];    

      req_send[k] = MPI::COMM_WORLD.Isend(features[k], size, MPI::INT, k + 1, Tag);
      req_recv[k] = MPI::COMM_WORLD.Irecv(res[k], 2, MPI::DOUBLE, k + 1, Tag);
    }

    // stopping criterium is the number of seconds 
    int i = 0;
    while (time(NULL) < finishtime) {
      bool flag = MPI::Request::Testany((int)np, req_recv, i, Stat);

      // receive one evaluation from i
      if (flag && i != MPI::UNDEFINED) {
        solutions[i].objective_vector.resize(2);
        solutions[i].objective_vector[0] = res[i][0];
        solutions[i].objective_vector[1] = res[i][1];
        
        // print the solution
        std::cout << solutions[i] << std::endl;

        // new random solution
        init(solutions[i]);

        // send/receive 
        for(unsigned int l = 0; l < size; l++) {
          features[i][l] = solutions[i][l];
        }
        
        req_send[i] = MPI::COMM_WORLD.Isend(features[i], size, MPI::INT, i + 1, Tag);
        req_recv[i] = MPI::COMM_WORLD.Irecv(res[i], 2, MPI::DOUBLE, i + 1, Tag);
      }
    }
    
    // end
    int j = 0;
    while (j < (int) np) {
      bool flag = MPI::Request::Testany((int)np, req_recv, i, Stat);
      if (flag && i != MPI::UNDEFINED) {
        solutions[i].objective_vector.resize(2);
        solutions[i].objective_vector[0] = res[i][0];
        solutions[i].objective_vector[1] = res[i][1];
        
        // print the solution
        std::cout << solutions[i] << std::endl;
        
        req_send[i] = MPI::COMM_WORLD.Isend(features[i], size, MPI::INT, i + 1, EndTag);
        j++;
      }
    }

    // free buffers
    for(unsigned k = 0; k < np; k++) {
      delete [] features[k];
      delete [] res[k];
    }

  }

protected:
  unsigned size;

  unsigned np;

  // number of seconds of the run
  time_t finishtime; 
  unsigned duration;

}; // end ms

#endif
