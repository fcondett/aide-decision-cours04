#ifndef _solution_h
#define _solution_h

#include <iostream>
#include <vector>

/**
   Classe pour représenter une solution
   
   Vector de VART={unsigned int, double, etc.} 
   auquel est ajoutée le vector des valeurs objectifs, et une valuer scalaire qui aggrége les objectifs
*/
template<class VART>
class Solution : public std::vector<VART> {
public:
  Solution() : std::vector<VART>() {
    scalar_fitness = 0.0;
  }

  Solution(const Solution<VART> & _s) : std::vector<VART>(_s) {
    objective_vector.resize(_s.objective_vector.size());

    for(unsigned int i = 0; i < _s.objective_vector.size(); i++)
      objective_vector[i] = _s.objective_vector[i];

    scalar_fitness = _s.scalar_fitness;
  }

  Solution& operator=(const Solution<VART> & _s) {
    this->resize(_s.size());
    for(unsigned int i = 0; i < _s.size(); i++)
      this->operator[](i) = _s[i];

    objective_vector.resize(_s.objective_vector.size());
    for(unsigned int i = 0; i < _s.objective_vector.size(); i++)
      objective_vector[i] = _s.objective_vector[i];

    scalar_fitness = _s.scalar_fitness;

    return *this;
  }

  /**
   * print the solution
   */
  virtual void printOn(std::ostream& _os) const {
    _os << objective_vector.size() ;
    for(unsigned i = 0; i < objective_vector.size(); i++)
      _os << " " << objective_vector[i] ;

    _os << " " << scalar_fitness ;

    _os << " " << this->size() ;

    for(unsigned int i = 0; i < this->size(); i++)
      _os << " " << this->operator[](i) ;

  }

  /**
   * print the solution
   */
  virtual void readFrom(std::istream& _is) {
    unsigned d;

    _is >> d;
    objective_vector.resize(d);

    for(unsigned i = 0; i < objective_vector.size(); i++)
      _is >> objective_vector[i] ;

    _is >> scalar_fitness ;

    _is >> d;
    this->resize(d);

    for(unsigned int i = 0; i < this->size(); i++)
      _is >> this->operator[](i);

  }

  // objective vector
  std::vector<double> objective_vector;

  // fitness value : scalar value of agregated objectives
  double scalar_fitness;

};

template<class VART>
std::ostream & operator<<(std::ostream& _os, const Solution<VART> & _s) {
    _s.printOn(_os);
    return _os;
}

template<class VART>
std::istream & operator>>(std::istream& _is, Solution<VART> & _s) {
    _s.readFrom(_is);
    return _is;
}

#endif
