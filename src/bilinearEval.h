#ifndef __bilinearEval
#define __bilinearEval

#include <solution.h>
#include <eval.h>

class BilinearEval : public Eval<Solution<unsigned int> > {
public:
    virtual void operator()(Solution<unsigned int> & _x) {
        double f1 = 0;
        double f2 = 0;

        for(unsigned int i = 0; i < _x.size(); i++) {
            if (_x[i] != i) f1++;
            if (_x[i] != _x.size() - 1 - i) f2++;            
        }

        _x.objective_vector[0] = _x.size() - f1;
        _x.objective_vector[1] = _x.size() - f2;
    }

};

#endif